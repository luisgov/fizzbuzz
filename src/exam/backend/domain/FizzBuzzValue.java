package exam.backend.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FizzBuzzValue {
	private final static Logger log = LoggerFactory.getLogger(FizzBuzzValue.class);
	private final static String VALUE_15 = "fizzbuzz";
	private final static String VALUE_3 = "fizz";
	private final static String VALUE_5 = "buzz";

	/**
	 * Return the value in Fizz/Buzz format
	 * @param value
	 * @return
	 */
	public String getValue(int value) {
		final String FUNC = "[getValue]";
		log.info(FUNC + " - Begin");
		log.debug(FUNC + " - intialValue=[" + value + "]");

		String resultValue = String.valueOf(value); 

		/*	Any number divisible by three is replaced by the word fizz. 
		 	Any number divisible by five is replaced by the word buzz. 
		 	Numbers divisible by both become fizzbuzz.
		 */
		if (value % 15 == 0) {
			resultValue = VALUE_15;
		} else if (value % 3 == 0) {
			resultValue = VALUE_3;
		} else if (value % 5 == 0) {
			resultValue = VALUE_5;
		}

		log.debug(FUNC + " - returnedValue=[" + resultValue + "]");
		log.info(FUNC + " - Finish");
		return resultValue;
	}
}
