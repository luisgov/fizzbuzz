package exam.backend.domain.exception;

public class FizzBuzzException extends Exception {
	private static final long serialVersionUID = -6165678676113045287L;
	
	public FizzBuzzException(String message) {
		super(message);
	}
	
    public FizzBuzzException(String message, Throwable cause) {
        super(message, cause);
    }
}
