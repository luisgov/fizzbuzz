 package exam.backend.application;

import java.util.List;

import com.google.gson.Gson;

public class ResponseMessage {
	@SuppressWarnings("unused")
	private String responseCode;
	@SuppressWarnings("unused")
	private String responseMessage;
	@SuppressWarnings("unused")
	private int start;
	@SuppressWarnings("unused")
	private int end;
	@SuppressWarnings("unused")
	private List<String> list;

	public ResponseMessage(String responseCode, String responseMessage) {
		this.responseCode = responseCode;
		this.responseMessage = responseMessage;
	}

	public ResponseMessage(String responseCode, String responseMessage, int start, int end, List<String> list) {
		this.responseCode = responseCode;
		this.responseMessage = responseMessage;
		this.start = start;
		this.end = end;
		this.list = list;
	}

	/**
	 * Returns the ResponseMessage in Json format
	 * @return
	 */
	public String toJson() {
		return new Gson().toJson(this);
	}
}
