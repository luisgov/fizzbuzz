package exam.backend.application;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import exam.backend.domain.FizzBuzzValue;
import exam.backend.domain.exception.FizzBuzzException;
import exam.backend.infrastructure.io.IWrite;

public class FizzBuzzList {
	private final static Logger log = LoggerFactory.getLogger(FizzBuzzList.class);
	private int minValue;
	private int maxValue;
	private IWrite writeFile;
	
	public FizzBuzzList(int min, int max, IWrite writeFile) {
		this.minValue = min;
		this.maxValue = max;
		this.writeFile = writeFile;
	}

	/**
	 * Recover the FizzBuzz list of numbers
	 * @return List of numbers
	 * @throws FizzBuzzException
	 */
	public List<String> getFizzBuzzList() throws FizzBuzzException {
		final String FUNC = "[getFizzBuzzList]";
		log.info(FUNC + " - Begin");
		List<String> result = new ArrayList<>();
		
		FizzBuzzValue fizzBuzzValue = new FizzBuzzValue();
		
        for (int i = this.minValue; i <= this.maxValue; i++)  {
			result.add(fizzBuzzValue.getValue(i));
        }

        // Write to file
        writeFile.write(String.join(",", result));
        
        log.info(FUNC + " - Finish");
        return result;
    }
}
