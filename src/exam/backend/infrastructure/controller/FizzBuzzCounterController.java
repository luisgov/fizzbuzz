package exam.backend.infrastructure.controller;

import java.util.List;
import java.util.Properties;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import exam.backend.application.FizzBuzzList;
import exam.backend.application.ResponseMessage;
import exam.backend.infrastructure.configuration.MaxValue;
import exam.backend.infrastructure.configuration.ReadConfigurationFile;
import exam.backend.infrastructure.configuration.ResultPath;
import exam.backend.infrastructure.io.WriteFile;

@Path("count")
public class FizzBuzzCounterController {
	private final static Logger log = LoggerFactory.getLogger(FizzBuzzCounterController.class);

	/**
	 * Count the Fizz/Buzz list from a min value to a max value
	 * @param minValue
	 * @return A Json file with the response and the list generated
	 */
	@GET
	@Path("/{minValue}")
	@Produces("application/json")
	public String count(@PathParam("minValue") String minValue) {
		final String FUNC = "[count]";
		
		log.info(FUNC + " - Begin");
		log.debug(FUNC + " - minValue [" + minValue + "]");

		//Read Properties file
		Properties properties;
		try {
			properties = new ReadConfigurationFile("main.properties").getFile();
		} catch (Exception e) {
			return new ResponseMessage("-1", e.getMessage()).toJson();
		}

		// Get min number from parameter
		int min;
		try {
			min = Integer.parseInt(minValue);
		} catch (NumberFormatException e) {
			log.error("-1 - Error: Min is not a number.", e);
			return new ResponseMessage("-2", "Error: Min is not a number.").toJson();
		}

		// Get max number from properties file
		int max;
		try {
			max = new MaxValue(properties).getValue();
		} catch (Exception e) {
			return new ResponseMessage("-3", e.getMessage()).toJson();
		}
		
		log.debug(FUNC + " - max [" + max + "]");
		
		// Check if min number less than max number
		if (min > max) {
			log.error("-3 - Error: Min value must be lower than Max value.");
			return new ResponseMessage("-4", "Error: Min value must be lower than Max value.").toJson();
		}

		// Get from the properties the path to the result file to be generated
		String resultPath;
		try {
			resultPath = new ResultPath(properties).getValue();
		} catch (Exception e) {
			return new ResponseMessage("-5", e.getMessage()).toJson();
		}
		
		// Calculate the list
		List<String> result;
		try {
			result = new FizzBuzzList(min, max, new WriteFile(resultPath)).getFizzBuzzList();
		} catch (Exception e) {
			return new ResponseMessage("-6", e.getMessage()).toJson();
		}

		log.info(FUNC + " - Finish");
		return new ResponseMessage("200", "Success.", min, max, result).toJson();
	}
}
