package exam.backend.infrastructure.configuration;

import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import exam.backend.domain.exception.FizzBuzzException;

public class MaxValue {
	private final static Logger log = LoggerFactory.getLogger(MaxValue.class);
	private Properties properties;
	
	public MaxValue(Properties properties) {
		this.properties = properties;
	}

	/**
	 * Get the Max value from the properties file
	 * @return
	 * @throws FizzBuzzException
	 */
	public int getValue() throws FizzBuzzException {
		final String FUNC = "[getValue]";
		log.info(FUNC + " - Begin");
		
 		int maxValue = 0;

		try {
			maxValue = new Integer(properties.getProperty("maxValue"));
		} catch (Exception e) {
			log.error("Error: maxValue is not valid.", e);
			throw new FizzBuzzException("-104 - Error: maxValue is not valid.", e);
		}

		log.info(FUNC + " - Finish");
		return maxValue;
	}
}
