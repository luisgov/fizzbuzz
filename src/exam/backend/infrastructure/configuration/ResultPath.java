package exam.backend.infrastructure.configuration;

import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import exam.backend.domain.exception.FizzBuzzException;

public class ResultPath {
	private final static Logger log = LoggerFactory.getLogger(ResultPath.class);
	private Properties properties;
	
	public ResultPath(Properties properties) {
		this.properties = properties;
	}

	/**
	 * Get the ResultPath value from the properties file
	 * @return
	 * @throws FizzBuzzException
	 */
	public String getValue() throws FizzBuzzException {
		final String FUNC = "[getValue]";
		log.info(FUNC + " - Begin");
		
 		String resultPath = "";

		try {
			resultPath = properties.getProperty("resultPath");
		} catch (Exception e) {
			log.error("Error: Path not found.", e);
			throw new FizzBuzzException("-103 - Error: resultPath not found.", e);
		}

		log.info(FUNC + " - Finish");
		return resultPath;
	}
}
