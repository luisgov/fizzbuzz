package exam.backend.infrastructure.configuration;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import exam.backend.domain.exception.FizzBuzzException;

public class ReadConfigurationFile {
	private final static Logger log = LoggerFactory.getLogger(ReadConfigurationFile.class);
	private String filename;
	
	public ReadConfigurationFile(String filename) {
		this.filename = filename;
	}

	/**
	 * Load the properties file
	 * @return
	 * @throws FizzBuzzException
	 */
	public Properties getFile() throws FizzBuzzException {
		final String FUNC = "[getFile]";
		log.info(FUNC + " - Begin");
		
		Properties properties = new Properties();

		try {
			properties.load(getClass().getClassLoader().getResourceAsStream(this.filename));
		} catch (FileNotFoundException e) {
			log.error("Error: configuration file not found.", e);
			throw new FizzBuzzException("-101 - Error: configuration file not found.", e);
		} catch (IOException e) {
			log.error("Error: Configuration file cannot be read.", e);
			throw new FizzBuzzException("-102 - Error: Configuration file cannot be read.", e);
		}
		
		log.info(FUNC + " - Finish");
		return properties;
	}
}
