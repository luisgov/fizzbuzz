package exam.backend.infrastructure.io;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import exam.backend.domain.exception.FizzBuzzException;

public class WriteFile implements IWrite {
	private final static Logger log = LoggerFactory.getLogger(WriteFile.class);
	
	private String filename;
	
	public WriteFile(String filename) {
		this.filename = filename;
	}
	
	@Override
	public void write(String list) throws FizzBuzzException {
		final String FUNC = "[write]";
		log.info(FUNC + " - Begin");
		log.debug(FUNC + " - filename=[" + filename + "]");
	
		try(FileWriter fw = new FileWriter(filename, true);
			    BufferedWriter bw = new BufferedWriter(fw);
			    PrintWriter out = new PrintWriter(bw))
		{
			// Get the current date and time 
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			
			// Write date, time and list to the file
		    out.println(sdf.format(date.getTime()) + " :: " + list + System.lineSeparator());
		    log.info("Saved file successful: " + filename);
		} catch (IOException e) {
			log.error("Error: writing in file.", e);
			throw new FizzBuzzException("-105 - Error: writing in file.", e);
		}		

		log.info(FUNC + " - Finish");
	}
}
