package exam.backend.infrastructure.io;

import exam.backend.domain.exception.FizzBuzzException;

public interface IWrite {
	/**
	 * Write the list in a file
	 * @param list
	 * @throws FizzBuzzException
	 */
	public void write(String list) throws FizzBuzzException;
}
