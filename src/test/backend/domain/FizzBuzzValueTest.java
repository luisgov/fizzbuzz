package test.backend.domain;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import exam.backend.domain.FizzBuzzValue;

public class FizzBuzzValueTest {

	@Test
	public void testGetValueFizz() {
		FizzBuzzValue fbv = new FizzBuzzValue();
		int value = 33;

		String expectedResult = "fizz";
		
		String result = fbv.getValue(value);

		assertEquals(expectedResult, result);
	}

	@Test
	public void testGetValueBuzz() {
		FizzBuzzValue fbv = new FizzBuzzValue();
		int value = 55;

		String expectedResult = "buzz";
		
		String result = fbv.getValue(value);

		assertEquals(expectedResult, result);
	}

	@Test
	public void testGetValueFizzBuzz() {
		FizzBuzzValue fbv = new FizzBuzzValue();
		int value = 90;

		String expectedResult = "fizzbuzz";
		
		String result = fbv.getValue(value);

		assertEquals(expectedResult, result);
	}
}
