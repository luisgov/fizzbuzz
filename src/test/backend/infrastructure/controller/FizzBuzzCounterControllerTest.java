package test.backend.infrastructure.controller;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

import exam.backend.infrastructure.controller.FizzBuzzCounterController;

public class FizzBuzzCounterControllerTest {

	@Test
	public void testCount() {
		FizzBuzzCounterController fbc = new FizzBuzzCounterController();
		String minValue = "22";

		String expectedResult = "{\"responseCode\":\"200\",\"responseMessage\":\"Success.\",\"start\":22,\"end\":27,\"list\":[\"22\",\"23\",\"fizz\",\"buzz\",\"26\",\"fizz\"]}";
		
		String result = fbc.count(minValue);

		assertEquals(expectedResult, result);
	}
}
